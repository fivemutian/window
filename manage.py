# !/usr/bin/evn python3

import os
from app.flask_init import create_app, db
from flask_script import Manager, Shell
from flask_migrate import Migrate, MigrateCommand
from app.models.user import User
from app.models.order import Order
from app.models.account import Account
from app.models.payment import Payment
from app.models.role import Role
from app.models.permission import Permission
from app.models.photo import Photo
from app.models.picture import Picture


app = create_app()
manager = Manager(app)
migrate = Migrate(app, db)

def make_shell_context():
    return dict(app=app, 
            db=db, 
            User=User,
            Order=Order,
            Account = Account,
            Payment = Payment,
            Role=Role,
            Permission=Permission,
            Photo=Photo,
            Picture = Picture
            )

manager.add_command("shell", Shell(make_context=make_shell_context))
manager.add_command("db", MigrateCommand)


if __name__ == "__main__":
    manager.run()
