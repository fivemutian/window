# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: ext.py
# Created Date: 2017-11-27 14:07:09
from .json_response import MyFlask
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()
app = MyFlask(__name__)

