# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: routes.py
# Created Date: 2017-12-19 21:07:28
from app.views.main import main
from app.views.auth import bp_auth as path
from app.views.orders import orders

class Route():
    @staticmethod
    def init_app(app):
        app.register_blueprint(main)
        app.register_blueprint(path)
        app.register_blueprint(orders)
