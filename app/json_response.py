# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: json_response.py
# Created Date: 2017-11-27 09:26:34

from flask import Response, jsonify, Flask

class JSONResponse(Response):
    @classmethod
    def force_type(cls, response, environ=None):
        if isinstance(response, dict):
            response = jsonify(response)
        return super(JSONResponse, cls).force_type(response, environ)

class MyFlask(Flask):
    response_class = JSONResponse
