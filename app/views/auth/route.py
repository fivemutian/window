# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: route.py
# Created Date: 2017-11-24 11:50:34
from . import bp_auth
from .auth import *

bp_auth.add_url_rule("/login", methods=["POST"], view_func=login)
bp_auth.add_url_rule("/signup", methods=["POST"], view_func=signup)
