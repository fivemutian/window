# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: __init__.py
# Created Date: 2017-11-24 11:49:30
from flask import Blueprint

bp_auth = Blueprint('auth', __name__, url_prefix="/auth")

from . import route
