# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: list_view.py
# Created Date: 2017-12-19 12:15:50
from flask.views import View
from flask import g, request
from app.models.user import User
from app.models.order import Order

class ListAPI(View):
    
    def __init__(self, template_name):
        self.name = template_name

    def users(self):
        users = User.query.all()
        return {'users': [user.to_json() for user in users], 'msg': "ok"}

    def orders(self):
        orders = Order.query.first()
        return {'order': orders.to_json()}

    def dispatch_request(self):
        if getattr(self, self.name):
            return getattr(self, self.name)()
        else:
            return {'msg': "nothing"}

    def upload(self):
        o = Order.query.first()
        f = request.files['file']
        f.save("/tmp/file")
        url = o.upload_image("/tmp/file")
        return dict(url=url, msg="ok", code=200)


