# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: users.py
# Created Date: 2017-11-24 10:06:25
from flask import g
from app.models.user import User


def users():
    return {"user": g.current_user, "msg": "ok", "code": 200}

def user(id):
    user = User.query.filter_by(id=id).first()
    if user:
        return {'user': user.to_json(), "msg": "ok", "code": 200}
    else:
        return {"msg": "用户不见了", "code": 404}


