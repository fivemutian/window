# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: errors.py
# Created Date: 2017-11-24 09:59:11
from . import main

@main.app_errorhandler(404)
def page_not_found(e):
    return {'msg': " 访问的路径不存在", 'code': 404}, 200

@main.app_errorhandler(400)
def no_method(e):
    print("参数请求失败{}".format(e))
    return {'msg': "参数请求失败", 'code': 400}, 200

@main.app_errorhandler(500)
def internal_server(e):
    print("服务器内部错误{}".format(e))
    return {"msg": "服务器内部错误", "code": 500}, 200
