# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: __init__.py
# Created Date: 2017-11-24 09:54:48
from flask import Blueprint

main = Blueprint('main', __name__)

from . import route, errors

