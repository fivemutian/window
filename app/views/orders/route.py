# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: route.py
# Created Date: 2017-12-26 14:14:15
from . import orders
from .orders_controller import OrdersView

orders.add_url_rule("/", view_func=OrdersView.index)
orders.add_url_rule("/<int:id>", view_func=OrdersView.show)
