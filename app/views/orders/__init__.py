# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: __init__.py
# Created Date: 2017-12-26 13:56:41
from flask import Blueprint

orders = Blueprint('orders',__name__, url_prefix='/orders')

from . import route
