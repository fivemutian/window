# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: orders.py
# Created Date: 2017-12-26 14:03:55
from flask import request, g
from app.models.order import Order
from . import orders
import re
class OrdersView():
    order = None
    def __init__(self, template_name=None):
        self.name = template_name

    def index():
        args = dict(page=request.args.get("page", 1))
        orders, page = Order.model_search(**args)
        return dict(orders=[order.to_json() for order in orders], page = page, msg='ok', code=200)
    def show(id):
        return dict(order=OrdersView.order.to_json(), msg='ok',code=200)

    @orders.before_request
    def find_order():
        match = re.compile(r"\d+")   
        temp = match.findall(request.path)
        if temp:
            t_id = temp[0]
            order = Order.query.filter_by(id=t_id).first()
            if not order:
                return dict(msg="该订单无效", code=404)
            OrdersView.order = order
