# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: picture.py
# Created Date: 2017-12-26 10:59:54
from app.extra import *

class Picture(db.Model, Modelable, Serialize):
    __tablename__ = 'pictures'
    id = db.Column(db.Integer, primary_key=True)
    image = db.Column(db.String, nullable=False)
    image_type = db.Column(db.String, default='order')
    order_id = db.Column(db.Integer, db.ForeignKey("orders.id"), index=True)

    def __repr__(self):
        return "<Picture id: {}, image: {}, imageable_type: {}, imageable_id: {}>".format(self.id, self.image, self.imageable_type, self.imageable_id)

    def __str__(self):
        return "picture"
