# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: user_role.py
# Created Date: 2017-12-20 13:55:36
from app.extra import *

user_roles = db.Table("user_roles",
        db.Column("user_id", db.Integer, db.ForeignKey("users.id")),
        db.Column("role_id", db.Integer, db.ForeignKey("roles.id"))
        )
