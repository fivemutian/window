# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: photo.py
# Created Date: 2017-12-26 10:50:33
from app.extra import *
import uuid
class Photo(db.Model, Serialize, Modelable):
    __tablename__ = 'photos'
    id = db.Column(db.Integer, primary_key=True)
    image = db.Column(db.String)

    def __repr__(self):
        return "<Photo id: {}, image: {}>".format(self.id, self.image)

    def __str__(self):
        return "photo"

    @classmethod
    def upload(cls, f):
        q = Auth(access_key=app.config.get("QINIU_AK"), secret_key=app.config.get("QINIU_SK"))
        key = uuid.uuid4()
        bucket_name = app.config.get("BUCKET_NAME")
        base_url = app.config.get("QINIU_URL")
        token = q.upload_token(bucket_name, key, 3600)
        ret, info = put_file(token, key, f)
        if info.status_code == 200:
            image = base_url + ret.get("key")
            p = cls(image=image)
            db.session.add(p)
            db.session.commit()
            return True, p
        else:
            return False, "上传失败"

