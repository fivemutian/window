# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: account.py
# Created Date: 2017-12-05 11:43:34
from app.extra import *

class Account(db.Model, Serialize):
    __tablename__ = "accounts"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, index=True, unique=True, nullable=False)
    updated_at = db.Column(db.Integer, default=time.mktime(datetime.utcnow().timetuple()), onupdate=time.mktime(datetime.utcnow().timetuple()))
    created_at = db.Column(db.Integer, default=time.mktime(datetime.utcnow().timetuple()))
    nickname = db.Column(db.String, index=True)
    manager_id = db.Column(db.Integer, index=True)
    roles = db.relationship("Role", backref="account", lazy="dynamic")
    permissions = db.relationship("Permission", backref="account", lazy="dynamic")


    def __repr__(self):
        return "<Account {}, {}, {}>".format(self.id, self.name, self.nickname)

    #我是否加入这个公司
    def is_join(self, user):
        return user in self.users
