# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: role.py
# Created Date: 2017-12-20 11:16:37
from app.extra  import *
from .role_permissions import role_permissions
from .user_role import user_roles
class Role(db.Model, Serialize):
    __tablename__ = "roles"

    id = db.Column(db.Integer, primary_key=True)
    account_id = db.Column(db.Integer, db.ForeignKey("accounts.id"), index=True)
    title = db.Column(db.String)
    level = db.Column(db.Integer, index=True, default=0x00)
    raty = db.Column(db.Float, default=0.00)
    updated_at = db.Column(db.Integer, default=time.mktime(datetime.utcnow().timetuple()), onupdate=time.mktime(datetime.utcnow().timetuple()))
    created_at = db.Column(db.Integer, default=time.mktime(datetime.utcnow().timetuple()))
    permissions = db.relationship("Permission", secondary=role_permissions, lazy="subquery", backref=db.backref("roles", lazy=True))
    users = db.relationship("User", secondary=user_roles, lazy="subquery", backref=db.backref("roles", lazy=True))


    def __repr__(self):
        return "<Role id: {}, title: {}, level: {}, account: {}>".format(self.id, self.title, self.level, self.account_id)

    
    def validate_role(self):
        pass


    
    
