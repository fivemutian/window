# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: role_permissions.py
# Created Date: 2017-12-20 11:30:03
from app.extra import *
role_permissions = db.Table("role_permissions",
            db.Column("role_id", db.Integer, db.ForeignKey("roles.id")),
            db.Column("permission_id", db.Integer, db.ForeignKey("permissions.id"))
        )
