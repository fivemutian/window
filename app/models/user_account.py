# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: user_account.py
# Created Date: 2017-12-20 09:10:47
from app.extra import *
#from app.models.user import User
#from app.models.account import Account

user_accounts = db.Table("user_accounts",
        db.Column("user_id", db.Integer, db.ForeignKey("users.id")),
        db.Column("account_id", db.Integer, db.ForeignKey("accounts.id"))
        )
