# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: permission.py
# Created Date: 2017-12-20 11:21:21
from app.extra import *

class Permission(db.Model, Serialize):
    __tablename__ = "permissions"
    id = db.Column(db.Integer, primary_key=True)
    account_id = db.Column(db.Integer, db.ForeignKey("accounts.id"), index=True)
    level = db.Column(db.Integer, default=0x00)
    title = db.Column(db.String)
    

    def __repr__(self):
        return "<Permission id: {}, title: {}, level: {}, account: {}>".format(self.id, self.title, self.level, self.account_id)

