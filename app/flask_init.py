# !/usr/bin/evn python3
# -*- coding: utf-8 -*-
# File Name: flask_init.py
# Created Date: 2017-12-25 10:42:55
from config import Config
from .ext import db, app
from .routes import Route

def create_app():
    app.config.from_object(Config)
    Config.init_app(app)
    db.init_app(app)
    Route.init_app(app)

    return app
